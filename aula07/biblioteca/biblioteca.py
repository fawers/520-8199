from typing import List, Union

from excecoes import LivroRuimError


class Livro:
    def __init__(self, nome: str, num_pags: int, categorias: List[str]):
        if num_pags <= 0:
            raise LivroRuimError(nome, num_pags)

        self.nome = nome
        self.num_pags = num_pags
        self.categorias = categorias

    def __str__(self):
        (nome, paginas, categorias) = (self.nome, self.num_pags, self.categorias)
        return f"Livro({nome=}, {paginas=}, {categorias=})"

    def __repr__(self):
        return str(self)

    def __eq__(self, other: 'Livro') -> bool:
        return (self.nome, self.num_pags, self.categorias) \
            == (other.nome, other.num_pags, other.categorias)

    def __ne__(self, other: 'Livro') -> bool:
        return not (self == other)


class Biblioteca:
    def __init__(self, *livros: Livro):
        self.estante = list(livros)

    def __iter__(self):
        return iter(self.estante)

    def __getitem__(self, index_ou_nome: Union[int, str]):
        if isinstance(index_ou_nome, int):
            return self.estante[index_ou_nome]

        else:
            nome = index_ou_nome.lower()
            for livro in self.estante:
                if livro.nome.lower() == nome:
                    return livro

        raise KeyError(index_ou_nome)

    def __delitem__(self, index_ou_nome: Union[int, str]):
        if isinstance(index_ou_nome, int):
            self.estante.pop(index_ou_nome)

        else:
            livro = self[index_ou_nome]
            self.estante.remove(livro)

    def __contains__(self, livro: Livro) -> bool:
        return livro in self.estante

    # iadd => biblioteca += livro
    def __iadd__(self, livro: Livro):
        self.estante.append(livro)
        return self

    def livros_por_categoria(self, categoria: str) -> List[Livro]:
        livros = []

        for livro in self:
            if categoria in livro.categorias:
                livros.append(livro)

        return livros

