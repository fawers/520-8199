import importado


def main():
    configuracao = {
        'host': 'seguro.porto.pollastrini',
        'porta': 11 * 5 + 7,
        'banco': 'cavagnoli'
    }

    con = importado.montar_string_conexao(**configuracao)
    print(con)


if __name__ == '__main__':
    main()
