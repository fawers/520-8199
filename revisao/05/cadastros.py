import csv
from typing import List, Optional

import excecoes

FILENAME = 'cadastros.csv'
DELIM = ';'
CABECALHO = ['Nome', 'Classe', 'Nivel', 'Vida', 'Magia']

# 2) Escreva um programa em python que realize um cadastro. Deverão ser coletadas as
# seguintes informações:
# • nome
# • classe
# • nível
# • vida
# • magia
# Os registros deverão ser armazenados em um arquivo CSV. Caso desejar manter o padrão
# brasileiro, o CSV será separado pelo caractere ;.
# 3) Implemente uma função de consulta no programa do exercício 2.
# 4) Implemente uma função de exclusão no programa do exercício 2.

class Registro:
    def __init__(self, nome: str, classe: str, nivel: int, vida: int, magia: int):
        self.nome = nome
        self.classe = classe
        self.nivel = nivel
        self.vida = vida
        self.magia = magia

    def __repr__(self):
        nome, classe, nivel, vida, magia = \
            self.nome, self.classe, self.nivel, self.vida, self.magia
        return f"Registro({nome=}, {classe=}, {nivel=}, {vida=}, {magia=})"

    @classmethod
    def importar_de_linha(cls, linha: List[str]):
        [nome, classe, nivel, vida, magia] = linha

        try:
            nivel = int(nivel)
            vida = int(vida)
            magia = int(magia)

        except ValueError:
            raise excecoes.LinhaRuimError(
                dict(nome=nome, classe=classe, nivel=nivel, vida=vida, magia=magia))

        return cls(nome, classe, nivel, vida, magia)

    def exportar_para_linha(self) -> List:
        return [self.nome, self.classe, self.nivel, self.vida, self.magia]

def ler_registros() -> List[Registro]:
    registros: List[Registro] = []

    try:
        with open(FILENAME) as arq:
            leitor = csv.reader(arq, delimiter=DELIM)
            cab = next(leitor)

            assert cab == CABECALHO

            registros = [Registro.importar_de_linha(linha) for linha in leitor]
            # for linha in leitor:
            #     registros.append(Registro.importar_de_linha(linha))

    except FileNotFoundError:
        return []

    return registros

def escrever_registros(registros: List[Registro]):
    with open(FILENAME, 'w') as arq:
        escritor = csv.writer(arq, delimiter=DELIM)

        escritor.writerow(CABECALHO)
        escritor.writerows(reg.exportar_para_linha() for reg in registros)

        # writerows acima equivalente a:
        # for reg in registros:
        #     escritor.writerow(reg.exportar_para_linha())

def cadastrar(registros: List[Registro]) -> Registro:
    # nome: str, classe: str, nivel: int, vida: int, magia: int
    hf = "==========================="

    try:
        print(hf)
        print("Cadastro de novo personagem")
        nome = input("Nome:   ")
        classe = input("Classe: ")
        nivel = int(input("Nível:  "))
        vida = int(input("Vida:   "))
        magia = int(input("Magia:  "))
        print(hf)

    except ValueError as ve:
        raise excecoes.CadastroError(ve.args[0].partition(": ")[2])

    r = Registro(nome, classe, nivel, vida, magia)
    registros.append(r)

    escrever_registros(registros)
    return r

def consultar(registros: List[Registro]) -> Optional[Registro]:
    nome = input("Buscar nome: ")

    return next(filter(lambda r: r.nome == nome, registros), None)
    # ^^^^ equivalente a:
    # for reg in filter(lambda r: r.nome == nome, registros):
    #     return reg

    # return None

def excluir(registros: List[Registro]):
    nome = input("Excluir por nome: ")

    reg = next(filter(lambda r: r.nome == nome, registros), None)

    if reg is not None:
        registros.remove(reg)
        escrever_registros(registros)

    return reg

def menu():
    registros = ler_registros()

    while True:
        # CADASTRO:
        # try:
        #     r = cadastrar(registros)

        # except excecoes.CadastroError as ce:
        #     print(f"Não foi possível concluir o cadastro;")
        #     print(f"{ce.args[0]} não é um número inteiro válido.")

        # else:
        #     print(f"{r} cadastrado com sucesso!")


        # CONSULTA:
        # r = consultar(registros)

        # if r is not None:
        #     print(f"Encontrei: {r}.")

        # else:
        #     print("Não encontrei um registro com o nome específicado.")


        # EXCLUSÃO
        r = excluir(registros)

        if r is not None:
            print(f"Encontrei e removi: {r}.")

        else:
            print("Não encontrei um registro com o nome específicado.")

        print()

def main():
    menu()

if __name__ == '__main__':
    main()
