import xml.etree.ElementTree as et
from typing import List, Tuple


def ler_admin_e_clientes(nome_arq: str) -> List[Tuple[str, int]]:
    with open(nome_arq, 'r') as f:
        tree = et.parse(f)

    saida = []
    root = tree.getroot()

    for reg in root.iterfind('registro'):
        saida.append((reg.findtext('administradora'),
                      int(reg.findtext('clientes'))))

    return saida


def encontrar_total_reclamacoes(nome_arq: str) -> int:
    saida = 0

    with open(nome_arq) as f:
        root = et.parse(f)

    #                        XPath
    for rec in root.iterfind('.//reclamacoes'):
        saida += int(rec.attrib['totais'])

    return saida

def main():
    print("foram feitas {} reclamações em 2017.".format(
        encontrar_total_reclamacoes(
            '/home/fabricio-werneck/Desktop/gravacao/520/8199/aula05/dataset.xml')
    ))


if __name__ == '__main__':
    main()
