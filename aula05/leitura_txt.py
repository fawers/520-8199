import io
import sys
import unicodedata
from typing import Dict

def contar_vogais(arquivo: io.TextIOWrapper):
    criterio = 'aeiouy'
    vogais: Dict[str, int] = {}

    for conteudo in arquivo:
        conteudo = conteudo.strip()

        for c in conteudo:
            c = unicodedata.normalize('NFD', c)[0]
            if c.lower() in criterio:
                if c in vogais:
                    vogais[c] += 1

                else:
                    vogais[c] = 1

    return vogais

def main():
    nome_arquivo = sys.argv[1]
    f = open(nome_arquivo) # file / filé
    vogais = contar_vogais(f)
    print(vogais)
    f.close()


if __name__ == '__main__':
    main()
