from typing import Tuple

# R G B
# 0 0 0 => PRETO
# 1 0 0 => VERMELHO
# 0 1 0 => VERDE
# 0 0 1 => AZUL
# 1 1 0 =>
# 1 1 1 => BRANCO

class PlacaTransito:
    # background / fundo
    BG = {
        'branco': '\x1b[47m',
        'vermelho': '\x1b[41m',
    }

    # foreground / frente
    FG = {
        'branco': '\x1b[1;37m',
        'preto': '\x1b[1;30m',
        'vermelho': '\x1b[1;31m',
    }

    RESET = '\x1b[0m'

    def __init__(self, texto: str, cores: Tuple[str, str]):
        self.texto = texto
        self.cores = cores

    def mostrar(self):
        (fg, bg) = self.cores
        frente = self.FG[fg]
        fundo = self.BG[bg]
        print(f"{fundo}{frente}{self.texto}{self.RESET}")

class PlacaPare(PlacaTransito):
    def __init__(self):
        super().__init__('PARE', ('branco', 'vermelho'))

class PlacaRestricao(PlacaTransito):
    def __init__(self, *, pode_parar: bool):
        # if pode_parar:
        #  texto = '\\'
        # else:
        #  text = "X"

        texto = ' \\ ' if pode_parar else ' X '
        super().__init__(texto, ('vermelho', 'branco'))

class PlacaLimiteVelocidade(PlacaTransito):
    def __init__(self, limite_velocidade: int):
        texto = f"{limite_velocidade} km/h"
        super().__init__(texto, ('preto', 'branco'))
        self.limite = limite_velocidade

pare = PlacaPare()

proibido_estacionar = PlacaRestricao(pode_parar=True)
proibido_parar = PlacaRestricao(pode_parar=False)

limite30 = PlacaLimiteVelocidade(30)
limite120 = PlacaLimiteVelocidade(120)

for placa in [pare, proibido_estacionar, proibido_parar, limite30, limite120]:
    placa.mostrar()
