from datetime import datetime, timedelta

class Corretor:
    def __init__(self, nome: str, matricula: str):
        self.nome = nome
        self.matricula = matricula

    def __str__(self):
        nome = self.nome
        matricula = self.matricula
        return f"Corretor({nome=}, {matricula=})"

class Apolice:
    def __init__(self, id: str, produto: str, responsavel: Corretor, data_expiracao: datetime):
        self.id = id
        self.prod = produto
        self.responsavel = responsavel
        self.expira_em = data_expiracao

    def __str__(self):
        return f"Apolice({self.id=}, {self.prod=}, {self.responsavel}) ({self.expirado()=})"

    def expirado(self) -> bool:
        return datetime.today() >= self.expira_em

    def renovar(self, nova_data_expicarao) -> 'Apolice':
        return Apolice(self.id, self.prod, self.responsavel, nova_data_expicarao)


aps = [
    Apolice("A120", "produto", Corretor("Aline Mendes", "MNDS"), datetime(2022, 5, 24)),
    Apolice("B324", "importação", Corretor("Guilherme Santos", "SNTS"), datetime(2030, 1, 1))]

for ap in aps:
    print(ap)
