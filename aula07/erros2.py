apolice = {
    'id': 'ap1',
    'produkt': 'tratamento de erros',
    'corretor': 'apolícia',
    'data_expiracao': [2022, 5, 26]  # comente esta linha para KeyError
}

try:
    apolice['data_expiracao'][3]

except KeyError as ke:
    print(f"não encontrei a chave {ke} no dicionário {apolice=}.")
    print(f"{ke.args=}")

except IndexError as ie:
    print(f"Acesso de lista não permitido")

print("programa continua...")
