class DadoError(Exception):
    pass

class FaceZeroError(DadoError):
    pass

class FaceNegativaError(DadoError):
    pass
