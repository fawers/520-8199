from datetime import date

class Pessoa:
    # métodos mágicos
    def __init__(self, data_nasc: date):
        #dateruim
        self.data_nasc = data_nasc

    def __str__(self):
        return f"Pessoa({self.data_nasc})"

    def __repr__(self):
        return str(self)

    # equal ==
    def __eq__(self, other: 'Pessoa') -> bool:
        return self.data_nasc == other.data_nasc

    # not equal !=
    def __ne__(self, other) -> bool:
        return not (self == other)

    # greater than / >
    def __gt__(self, other: 'Pessoa') -> bool:
        return self.data_nasc > other.data_nasc

    # greater than or equal / >=
    def __ge__(self, other):
        return self == other or self > other

    # less than / <
    def __lt__(self, other):
        return not (self >= other)

    # less than or equal / <=
    def __le__(self, other):
        return not (self > other)

    # callable
    def __call__(self):
        print(f"chamado: {self}")

    def __or__(self, other: 'Pessoa'):
        if self > other:
            return self
        else:
            return other

    def mais_novo(self, other: 'Pessoa') -> 'Pessoa':
        return self | other

andre = Pessoa(date(1986, 8, 1))
henrique = Pessoa(date(1993, 12, 22))
santos = Pessoa(date(1994, 6, 2))
ruivo = Pessoa(date(1995, 7, 29))
jean = Pessoa(date(1991, 11, 23))
mendes = Pessoa(date(1985, 12, 26))
jessica = Pessoa(date(1994, 9, 26))
eveLLyn = Pessoa(date(1998, 11, 28))
hadad = Pessoa(date(1978, 3, 10))
fabricio = Pessoa(date(1992, 4, 4))

# print(f"{santos > jessica=}")
# print(f"{hadad < mendes=}")
# print(f"{jean == ruivo=}")
# print(f"{fabricio >= eveLLyn=}")

grupo = [
    andre,
    henrique,
    santos,
    ruivo,
    jean,
    mendes,
    jessica,
    eveLLyn,
    hadad,
    fabricio,
]

print(grupo)
print()
print(sorted(grupo))
print()
print(sorted(grupo, reverse=True))

andre()

print(henrique | jean)
print(mendes | jessica | eveLLyn | hadad)
print(hadad.mais_novo(eveLLyn.mais_novo(jessica.mais_novo(mendes))))
