class RPGError(Exception):
    pass

class CadastroError(RPGError):
    pass

class LinhaRuimError(RPGError):
    pass
