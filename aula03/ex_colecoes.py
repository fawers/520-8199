def capitulos_de_livros() -> list[str]:
    caps: list[str] = []

    ...

    return caps


def colocar_nums_capitulos(capitulos: list[str]) -> list[tuple[int, str]]:
    return []


c = capitulos_de_livros()
print(c)
print(colocar_nums_capitulos(c))
