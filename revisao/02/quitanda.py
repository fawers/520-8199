from typing import Dict

quitanda = {'Banana': 3.50, 'Melancia': 7.50, 'Morango': 5.00}
cesta: Dict[str, int] = {}

print("Bem-vindo à quitanda da cobra! (python)")

while True:
    print('1: Ver cesta')
    print('2: Adicionar frutas')
    print('3: Checkout')
    print('4: Sair')

    entrada = input('Digite a opção desejada: ')

    if entrada == '4':
        break

    elif entrada == '1':
        print(cesta)

    elif entrada == '2':
        print()
        i = 1
        for fruta in quitanda:
            print(f"{i}: {fruta}")
            i += 1

        fruta = input("Escolha a fruta desejada: ")

        if fruta := {'1': 'Banana', '2': 'Melancia', '3': 'Morango'}.get(fruta):
            if fruta in cesta:
                cesta[fruta] += 1
                #cesta[fruta] = cesta[fruta] + 1

            else:
                cesta[fruta] = 1

        else:
            print("Ainda não tenho uma fruta neste índice ;(")

    elif entrada == '3':
        total = 0.0

        for (fruta, qtd) in cesta.items():
            preco = quitanda[fruta]
            total += preco * qtd

        print(f"Total de compras: {total:.2f}")
        print("Cesta de compras:", ', '.join(cesta.keys()))

    else:
        print('Digite uma opção válida.')

    print()

print("Volte sempre!")
