# para executar:
# python -m unittest
# ^ na pasta onde se encontra test_dados.py

import unittest

import dados


class TestDado(unittest.TestCase):
    def test_dado_de_6_faces_rola_numeros_de_1_a_6(self):
        numeros_validos = range(1, 7)
        dado = dados.Dado(6)

        for _ in range(100):
            self.assertIn(dado.rolar(), numeros_validos)
            # rolamento in numeros_validos?
            # se NÃO: código quebrado

    def test_dado_viciado_rola_sempre_4(self):
        dado = dados.DadoViciado(4)

        for _ in range(100):
            self.assertEqual(dado.rolar(), 4)

    def test_dado_de_21_faces_rola_todas_as_faces_em_42_rolamentos(self):
        d21 = dados.novo_dado_com_semente(faces=21, semente=5)
        rolamentos = [True] + [False] * d21.faces # rolamentos[21]

        for _ in range(41):
            roll = d21.rolar()
            rolamentos[roll] = True

        self.assertTrue(all(rolamentos), msg=f"{dict(enumerate(rolamentos))=}")

