def dict_examples1():
    d = {'nome arquivo': __file__}
    print(d)

    d["nome módulo"] = __name__  # __main__
    d['linha arquivo'] = 6
    print(d)

    print(d.setdefault("chave", "valor"))
    print(d.setdefault("chave", "default"))

    print(d['linha arquivo'])
    print(d.get('linha arquivo'))
    print('CIget:', d.get('chave inexistente', 'valor inválido'))
    print('CI[]:', d['chave inexistente'])


def dict_examples2():
    d1 = {'países': ['brasil', 'irlanda']}
    d2 = d1.copy()
    d3 = d2

    d3['países'].append('namíbia')

    print("d1:", d1)
    print("d2:", d2)
    print("d3:", d3)

    print("d1 is d2?", d1 is d2)
    print("d1['países'] is d2['países']?", d1['países'] is d2['países'])


def dict_examples3():
    d = {'capital': 'rio de janeiro'}
    d.update(capital='brasilia')

    print(d)
    d.clear()

    d.update({'db': 'mysql'}, linux='archlinux')
    print(d)

    (chave, valor) = d.popitem()
    print(chave)
    print(valor)

    db = d.pop('db')
    print("db:", db)

    print(d)

    db = d.pop('db', 'sqlite3')
    print("db:", db)


def dict_examples4():
    d = dict(
        a=1, b=2)

    print(d)

    # mesmo que d.keys()
    for chave in d:
        print(f"chave: {chave}, valor: {d[chave]}")

    print("\n.keys()")
    for chave in d.keys():
        print(f"chave: {chave}, valor: {d[chave]}")

    print("\n.values()")
    for valor in d.values():
        print(f"valor: {valor}")

    print("\n.items()")
    for (chave, valor) in d.items():
        print(f"chave: {chave}, valor: {valor}")


dict_examples4()
