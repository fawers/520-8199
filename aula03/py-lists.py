def list_examples1():
    # mutáveis
    l1 = []
    l1.extend(['Sorvete', 'Chocolate', 'Cheetos'])
    l1.append('Arroz')
    print(l1)
    l1.append('Feijão')
    print(l1)
    l1.extend(['Pão', 'Manteiga', 'Geléia'])
    # l1.append('Pão')
    # l1.append('Manteiga')
    # l1.append('Geléia')
    print(l1)
    l1.append(['Leite', 'Cereal'])
    print(l1)

    for item in l1:
        if isinstance(item, str):
            print(item.upper())

        else:
            print(item)
            print("^^^ não é uma str\n")


def list_examples2():
    l1: list[str] = ["str", "int", "float", "complex"]
    l2 = l1.copy()
    l3 = l1

    l2.extend(["tuple", "list"])
    l3.append("dict")

    print("l1:", l1)
    print("l2:", l2)
    print("l3:", l3)

    print("\nl1 is l2:", l1 is l2)
    print("l1 is l3:", l1 is l3)
    print("l3 is l2:", l3 is l2)

    print(f"\nendereço de l1: {id(l1):x}")
    print(f"endereço de l2: {id(l2):x}")
    print(f"endereço de l3: {id(l3):x}")

def list_examples3():
    rua = ['ca5a', 'cb5a', 'cd5a']
    print(rua)
    rua.insert(0, 'c95a')
    rua.insert(3, 'cc5a')
    print(rua)
    rua[0] = 'w95'
    print(rua)

    print(rua.count('ca5a'))
    rua.append('ca5a')
    print(rua.count('ca5a'))

    # criar uma nova lista e colocá-la em rua2
    rua2 = sorted(rua)
    print("rua is rua2?", rua is rua2)
    print("rua == rua2?", rua == rua2)
    print(rua, rua2)

    # ordena a lista modificando-a
    rua.sort()
    print(rua)
    print("rua is rua2?", rua is rua2)
    print("rua == rua2?", rua == rua2)

    rua3 = sorted(rua, reverse=True)
    rua2.sort(reverse=True)

    print(rua3)
    print(rua is rua3, rua2 is rua3)

    rua3.reverse()


def list_examples4():
    nums = list(range(50))
    alvo = 25
    # print(f"o número {alvo} se encontra na posição {nums.index(alvo)} da lista")
    alvo = 55
    # print(f"mas e o número {alvo}, está na lista? {alvo in nums}")

    tamanho = len(nums)  # length / tamanho
    indice_meio = tamanho // 2 - 1

    fatia = nums[indice_meio-5:indice_meio+5]
    # ^^^ efetivamente a mesma coisa que:
    # fatia = [
    #     nums[indice_meio-5],
    #     nums[indice_meio-4],
    #     nums[indice_meio-3],
    #     nums[indice_meio-2],
    #     nums[indice_meio-1],
    #     nums[indice_meio],
    #     nums[indice_meio+1],
    #     nums[indice_meio+2],
    #     nums[indice_meio+3],
    #     nums[indice_meio+4],
    # ]
    # print(fatia)

    # for elem in fatia:
        # print(elem)

    fatia.clear()
    # print(fatia)

    print(nums.pop())
    print(nums.pop())
    print(nums.pop())
    print(nums.pop(0))
    print(nums.pop(0))
    print(nums.pop(0))
    print(nums.pop(
        len(nums) // 2
    ))
    print(nums.pop(
        len(nums) // 2
    ))
    print(nums.pop(
        len(nums) // 2
    ))

    # enquanto nums tiver pelo menos um elemento
    while nums:
        print(nums.pop(), end=', ')

    print("\npós loop:", nums)

list_examples4()
