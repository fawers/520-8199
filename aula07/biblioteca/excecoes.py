class BibliotecaError(Exception):
    pass

class LivroRuimError(BibliotecaError):
    pass

class LivroAusenteError(BibliotecaError):
    pass
