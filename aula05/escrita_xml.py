import json
import xml.etree.ElementTree as et

CONVERSOES = {
    'Ano': "@ano",
    'Semestre': '@semestre',
    'Índice': '@indice',
    'CNPJ AC': 'cnpj',
    'Administradora de consórcio': 'administradora',
    'Quantidade de clientes – Consorciados': 'clientes',
    'Quantidade de reclamações não reguladas': 'reclamacoes.nao-reguladas',
    'Quantidade de reclamações reguladas - outras': 'reclamacoes.reguladas-nao-procedentes',
    'Quantidade de reclamações reguladas procedentes': 'reclamacoes.reguladas-procedentes',
    'Quantidade total de reclamações': 'reclamacoes.@totais'
}

def ler_json(nome_arq: str):
    with open(nome_arq) as f:
        regs = json.load(f)

    return regs

def escrever_xml(nome_arq: str, regs):
    root = et.Element('registros')

    for reg in regs:
        registro = et.Element('registro')
        reclamacoes = et.Element('reclamacoes')

        for (chave, valor) in reg.items():
            campo = CONVERSOES[chave]
            (subelemento, _, dado) = campo.rpartition('.')

            if subelemento != 'reclamacoes':
                elem = registro

            else:
                elem = reclamacoes

            if dado.startswith('@'):
                elem.attrib[dado[1:]] = str(valor)

            else:
                e_dado = et.Element(dado)
                e_dado.text = str(valor)
                elem.append(e_dado)

        registro.append(reclamacoes)
        root.append(registro)

    with open(nome_arq, 'w') as f:
        et.indent(root)
        tree = et.ElementTree(root)
        tree.write(nome_arq)

def main():
    regs = ler_json("/home/fabricio-werneck/Desktop/gravacao/520/8199/aula05/dataset.json")
    escrever_xml("/home/fabricio-werneck/Desktop/gravacao/520/8199/aula05/dataset.xml", regs)

if __name__ == '__main__':
    main()
