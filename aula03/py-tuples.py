def tuple_examples1():
    # imutáveis
    vazia = ()
    print(vazia)

    tup1elem = (1,)
    print(type(tup1elem))
    print(tup1elem)

    tup2elem = (2, 3)
    print(tup2elem)

    tup3elem = tup1elem + tup2elem
    print(tup3elem)
    print("tup3 é tup2?", tup3elem is tup2elem)
    print("tup3 é tup1?", tup3elem is tup1elem)

def tuple_examples2():
    t = (8199, ['Mendes', 'Firmo', 'Ruivo', 'EveLLyn', 'Jessica', 'Edvaldo'])
    # desmontar / desestruturar / quebrar
    (turma, [primeiro, *resto, ultimo]) = t

    # print(turma)
    # print(primeiro)
    # print(ultimo)
    # print(resto)

    turmas = [
        (840, ['Fulano', 'Ciclano']),
        (420, ['Beltrano'])
    ]

    # for turma in turmas
    for (id, nomes) in turmas:
        # id = turma[0]
        # nomes = turma[1]
        print(id)
        print(nomes)
        print()

tuple_examples2()
