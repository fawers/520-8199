# ex01
def saudar(nome: str = 'mundo'):
    print(f"Olá, {nome}!")

def levar(quem: str, /, *, para: str):
    print(f"Vou levar {quem} para {para}!")


def ex02():
    operacoes = {
        '+': lambda a, b: a + b,
        '-': lambda a, b: a - b,
        '*': lambda a, b: a * b,
        '/': lambda a, b: a / b,
    }

    a = int(input("Digite o primeiro número: "))
    b = int(input("Digite o segundo número:  "))
    o = input('Digite a operação: ')

    if o in operacoes:
        print(f"{a} {o} {b} = {operacoes[o](a, b)}")

    else:
        print(f'Desconheço esta operação: {o}')

def ex02_2():
    operacoes = {
        '+': lambda a, b: a + b,
        '-': lambda a, b: a - b,
        '*': lambda a, b: a * b,
        '/': lambda a, b: a / b,
    }

    print("Escreva a expressão no formato NUMERO1 OPERADOR NUMERO2")
    [a, o, b] = input("> ").split()
    a, b = int(a), int(b)
    # expressao = input("> ").split()
    # a = int(expressao[0])
    # b = int(expressao[2])
    # o = expressao[1]

    if o in operacoes:
        print(f"{a} {o} {b} = {operacoes[o](a, b)}")

    else:
        print(f'Desconheço esta operação: {o}')


if __name__ == '__main__':
    # levar('Fulano', para='a praia')
    ex02_2()
