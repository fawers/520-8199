import unittest

import biblioteca


class TestBiblioteca(unittest.TestCase):
    def setUp(self):
        self.livros = [
            biblioteca.Livro("Python Fundamentals", 220, ['programação', 'iniciantes']),
            biblioteca.Livro("Programação com Scala", 450, ['funcional', 'programação'])]
        self.biblio = biblioteca.Biblioteca(*self.livros)

    def test_biblio_contem_2_livros_com_categoria_programacao(self):
        esperado = [
            biblioteca.Livro("Python Fundamentals", 220, ['programação', 'iniciantes']),
            biblioteca.Livro("Programação com Scala", 450, ['funcional', 'programação'])]

        self.assertEqual(self.biblio.livros_por_categoria('programação'), esperado)

    def test_biblio_contem_1_livro_com_categoria_iniciantes(self):
        esperado = [
            biblioteca.Livro("Python Fundamentals", 220, ['programação', 'iniciantes'])]

        self.assertEqual(self.biblio.livros_por_categoria('iniciantes'), esperado)

    def test_biblio_nao_contem_livros_com_categoria_teorico(self):
        esperado = []
        self.assertEqual(self.biblio.livros_por_categoria('teórico'), esperado)

    def test_iadd_adiciona_livros_a_biblioteca(self):
        # __iadd__ é +=
        livro = biblioteca.Livro('Senhor dos Pastéis', 800, ['fantasia', 'guerra', 'drama'])

        self.assertNotIn(livro, self.biblio)

        # self.biblio.__iadd__(livro)
        self.biblio += livro

        self.assertIn(livro, self.biblio)

    def test_biblio_com_indice_0_retorna_livro_de_python(self):
        self.assertEqual(self.biblio[0], self.livros[0])

    def test_biblio_com_chave_pf_retorna_livro_de_python(self):
        self.assertEqual(self.biblio['Python Fundamentals'], self.livros[0])

    def test_biblio_com_indice_invalido_resulta_em_index_error(self):
        with self.assertRaises(IndexError):
            self.biblio[3]

    def test_biblio_com_chave_invalida_resulta_em_key_error(self):
        with self.assertRaises(KeyError):
            self.biblio['Harry Podre']


class TestLivro(unittest.TestCase):
    def test_livro_e_criado_corretamente(self):
        biblioteca.Livro("Um livro", 10, [])

    def test_livro_nao_e_criado_corretamente(self):
        with self.assertRaises(biblioteca.LivroRuimError) as o:
            biblioteca.Livro("Um outro livro", -10, [])
            self.assertTrue(o.exception.args[1], -10)
