entrada = input("Digite um número: ")

# if
try:
    numero = int(entrada)
    divisao = 10 / numero

# elif
except ValueError as ve:
    print(f"Entrada não é um número válido. {entrada=}")

# elif
except ZeroDivisionError as zde:
    print("Jamais dividirás por zero!")

except Exception as e:
    log = {'erro': e.__class__.__name__,
           'args': e.args}
    print(f"{log=}")

# else
else:
    print(f"{numero=}")
    print(f"{divisao=}")

print("Próxima instrução.")
