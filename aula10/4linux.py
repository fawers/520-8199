# https://www.sqlite.org/docs.html
import os.path
import json
import sqlite3

PREFIX = '4linux.%s'
JSON_FILE = PREFIX % 'json'
DB_NAME = PREFIX % 'db'
SQL_FILE = PREFIX % 'sql'

SQL_INSERT = "INSERT INTO {tabela} VALUES ({valores});"
# INSERT INTO cursos VALUES (4520, "Python Fundamentals");
# INSERT INTO turmas VALUES (8199, 4520, "Fabricio");
# INSERT INTO membros VALUES (8199, "Aline Hadad");

SQL_SELECT = "SELECT {colunas} FROM {tabela} {extra};"
# SELECT t.professor, m.nome FROM membros m JOIN turma t on m.turma_id = t.id;


def primeira_execucao() -> bool:
    print("\x1b[34mcomeço\x1b[0m")
    if os.path.exists(DB_NAME):
        print("\x1b[31mbanco já existe\x1b[0m")
        return False

    with open(JSON_FILE) as json_file:
        dados = json.load(json_file)
    print("\x1b[33mjson carregado\x1b[0m")

    with sqlite3.connect(DB_NAME) as db:
        print("\x1b[32mconexão aberta\x1b[0m")
        with open(SQL_FILE) as sql_file:
            db.executescript(sql_file.read())

        print("\x1b[35mtabelas criadas\x1b[0m")

        for (tabela, linhas) in filter(lambda par: not par[0].endswith('cols'),
                                       dados.items()):
            num_cols = dados[f"{tabela}_cols"]
            valores = ', '.join('?' * num_cols)
            insert = SQL_INSERT.format(tabela=tabela, valores=valores)
            # INSERT INTO cursos VALUES (?, ?);

            print(f"{insert = }\n{linhas = }")
            db.executemany(insert, linhas)
            print(f"\x1b[36mlinhas inseridas\x1b[0m")

    print("\x1b[42;37mconexão fechada\x1b[0m")

    return True

def membros_por_professor(professor: str):
    tabela = 'membros m'
    colunas = 't.professor, m.nome'
    extra = 'JOIN turmas t ON t.id = m.turma_id WHERE t.professor = ?'

    select = SQL_SELECT.format(tabela=tabela, colunas=colunas, extra=extra)
    # print(f"POR PROFESSOR {select=}")
    with sqlite3.connect(DB_NAME) as db:
        cur = db.execute(select, [professor])

        for linha in cur:
            print(f"{linha=}")

def membros_por_curso(curso_id: int):
    tabela = 'membros m'
    colunas = 'c.nome, t.id, m.nome'
    extra = ('JOIN turmas t ON t.id = m.turma_id ' +
             'JOIN cursos c ON c.id = t.curso_id ' +
             'WHERE c.id = ?')

    select = SQL_SELECT.format(tabela=tabela, colunas=colunas, extra=extra)
    # print(f"POR CURSO {select=}")
    with sqlite3.connect(DB_NAME) as db:
        cur = db.execute(select, [curso_id])

        for linha in cur:
            print(f"{linha=}")

def main():
    primeira_execucao()
    print()
    membros_por_professor('Hector Vido')
    print()
    membros_por_curso(4520)

if __name__ == '__main__':
    main()
