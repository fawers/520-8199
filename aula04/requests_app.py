import requests

ENDPOINT = "https://rickandmortyapi.com/api"
ENDPOINT_PERSONAGENS = f"{ENDPOINT}/character"

def main():
    rick = requests.get(ENDPOINT_PERSONAGENS + '/1')
    print(rick.json())

if __name__ == '__main__':
    main()
