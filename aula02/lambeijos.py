nomes = [
    'Hadad', 'Ruivo',
    'Campos', 'Silva (com 2 L?)',
    'Mendes', 'Firmo'
]

nomes_maiusculos = list(map(lambda s: s.upper(), nomes))

# nomes_maiusculos_handmade = []

# for nome in nomes:
#     nomes_maiusculos_handmade.append(
#         (lambda s: s.upper())(nome)
#     )

def string_impar(s: str) -> bool:
    # length / tamanho
    return len(s) % 2 != 0

nomes_impares = list(filter(lambda s: len(s) % 2 != 0, nomes))

nomes_impares_handmade = []

for nome in nomes:
    if string_impar(nome):
        nomes_impares_handmade.append(nome)

print('m:', nomes)
print('M:', nomes_maiusculos)
print('F:', nomes_impares)
print('FF', nomes_impares_handmade)
# print('H:', nomes_maiusculos_handmade)

