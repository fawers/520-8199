import flask

app = flask.Flask(__name__)

@app.route("/")
def home():
    return "OLÁ MUNDO DA WEB"


@app.route("/<name>")
def greet(name):
    return f"Olá, <i>{name}</i>!"


@app.route("/flask")
def flaskk():
    return "<h1>USEM FLASK</h1>"


if __name__ == '__main__':
    app.run(debug=True)
