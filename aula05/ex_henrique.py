import os.path

def mostrar_menu(titulo: str, opcoes: list[str]):
    print(titulo)

    for op in opcoes:
        print(f"- {op}")

    entrada = ''

    while entrada not in opcoes:
        entrada = input("Digite a opção desejada: ")

    return entrada

def sugestao1(*colunas):
    registro = '{};{};{};{};{}'.format(*colunas)
    #                                  'cpf', 'nome', bla

    with open('cadastro.csv', 'r') as arquivo:
        conteudo = arquivo.readlines()
        conteudo.append(registro)

    with open('cadastro.csv', 'w') as arquivo:
        arquivo.writelines(conteudo)
        arquivo.write('\n')


CABECALHO = 'CPF;Nome;Idade;Sexo;Endereco'

def sugestao2(*colunas):
    if os.path.exists('cadastro.csv'):
        with open('cadastro.csv') as arquivo:
            registros = arquivo.readlines()

    else:
        registros = [CABECALHO]

    registro = '{};{};{};{};{}'.format(*colunas)
    registros.append(registro)

    with open('cadastro.csv', 'w') as arquivo:
        for registro in registros:
            arquivo.write(registro.strip() + '\n')

def inserir_nomes_colunas():
    cpf = 'CPF'
    nome = 'Nome'
    idade = 'Idade'
    sexo = 'Sexo'
    endereco = 'Endereco'

    sugestao1(cpf, nome, idade, sexo, endereco)
    return
    # original abaixo
    registro = '{};{};{};{};{}'.format(cpf, nome, idade, sexo, endereco)

    with open('cadastro.csv', 'r') as arquivo:
        conteudo = arquivo.readlines()
        conteudo.append(registro)

    with open('cadastro.csv', 'w') as arquivo:
        arquivo.writelines(conteudo)
        arquivo.write('\n')

def inserir_registro():
    cpf = input('CPF: ')
    nome = input('Nome: ')
    idade = input('Idade: ')
    sexo = input('Sexo (m/f): ')
    endereco = input('Endereço: ')

    sugestao2(cpf, nome, idade, sexo, endereco)

    return # original abaixo
    registro = '{};{};{};{};{}'.format(cpf, nome, idade, sexo, endereco)

    arquivo = open('cadastro.csv', 'r')
    conteudo = arquivo.readlines()
    conteudo.append(registro)
    arquivo.close()

    arquivo = open('cadastro.csv', 'w')  # append
    arquivo.writelines(conteudo)
    arquivo.write('\n')
    arquivo.close()

def main():
    entrada = ''

    while entrada != "sair":

        entrada = mostrar_menu(
            "===== Inserir dados arquivo .csv =====",
            ['adicionar','sair'])

        if entrada == 'adicionar':
            inserir_registro()


if __name__ == '__main__':
    main()
