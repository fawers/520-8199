import sys
import time
import random
from typing import List

def contar_rodada(segundos: int):
    print("Parando música em ", end='')
    for contagem in range(segundos, 0, -1):
        print(f"{contagem}... ", end='', flush=True)
        time.sleep(1)

def jogo_das_cadeiras(participantes: List[str],
                      segs_min: int = 2, segs_max: int = 6):
    assert len(participantes) > 1, "o jogo precisa de pelo menos 2 participantes"
    # if len(participantes) < 2:
    #     raise ValueError('o jogo precisa de pelo menos 2 participantes')

    r = random.Random()

    for cadeiras in range(len(participantes)-1, 0, -1):
        print(f"Restam \x1b[34m{cadeiras}\x1b[0m cadeiras no jogo.")
        contar_rodada(r.randint(segs_min, segs_max))
        removido = r.choice(participantes)
        participantes.remove(removido)

        print(f"\x1b[31m{removido}\x1b[0m ficou de fora!")

    assert len(participantes) == 1, f"{participantes=}"
    print(f"\x1b[32m{participantes[0]}\x1b[0m venceu o jogo!")

def main():
    participantes = sys.argv[1:]
    assert len(participantes) > 2, f'indique os participantes via linha de comando.'
    print(f"{participantes=}")
    jogo_das_cadeiras(participantes)


if __name__ == '__main__':
    main()
