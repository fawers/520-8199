string_ou_texto = "\""
string_ou_texto2 = '"'
string_multilinha = """

"""
string_multilinha2 = '''

'''
meu_nome = "Fabricio Werneck"
minha_idade = "30"
nome_curso = "520 " + "Python Fundamentals"
nome_curso.endswith("Fundamentals")

verdadeiro = True
falso = False

numeros_inteiros = 0
somas = numeros_inteiros + 5
subtracoes = somas - 2
multiplicacoes = subtracoes * 17
divisoes_inteiras = multiplicacoes // 6
resto_divisao = multiplicacoes % 6
divisoes_racionais = multiplicacoes / 6
potencia = 2 ** 5
# print(potencia)

pi = 3.14 # R$ 1.000,00
dobro = pi * 2.7
segunda_soma = dobro + 10.5
divisoes_quebradas = segunda_soma / 1000000
# print("dobro:", dobro)
# print("uma conta meio doida:", pi * 7 / dobro ** (16 + 100))
# print("10 à 3a:", 1e3)
# print("4700:", 47e2)

listas = [1, 2, 3]
lista_nomes = ["Desenvolvimento com Go", "Python Fundamentals"]
lista_mistureba = [3.14, 10, "string", False]
tuplas = () + (1,2) + (3,)
curso_id_nome = (520, "Python Fundamentals")

dicionarios = {
    "nome": "Fabricio",
    "idade": 30
}

root = {
    "cursos": [
        {"nome": "Python Fundamentals", "id": 540},
        {"nome": "Desenvolvimento com Go", "id": 220}
    ],
    "professores": [
        "Fabricio",
        "Guilherme"
    ],
    "empresa": {
        "nome": "4Linux",
        "parceiros": [
            {"nome": "Porto"}
        ]
    }
}
# Ctrl Shift P

print(root['cursos'][0]["id"])
cursos = root['cursos']
primeiro_curso = cursos[1]
id_curso = primeiro_curso['id']
print(id_curso)
# print(dicionarios['nome'])

# Pra escrever algo para nossos coleguinhas
# ou para nós eus do futuro.

# E (AND)
# p | q | p E q
# V | V |   V
# V | F |   F
# F | V |   F
# F | F |   F

# OU (OR)
# p | q | p E q
# V | V |   V
# V | F |   V
# F | V |   V
# F | F |   F

# NÃO (NOT)
# p | NÃO p
# V | F
# F | V

idade = 16
eh_maior_de_idade = idade >= 18
esta_bebendo = False
pode_dirigir = eh_maior_de_idade and not esta_bebendo
print("pode dirigir?", pode_dirigir)
