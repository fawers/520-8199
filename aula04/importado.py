def montar_string_conexao(host: str, porta: int, banco: str) -> str:
    return f"mongodb://{host}:{porta}/{banco}"


def principal():
    host = input("Digite o host: ") or 'localhost'
    porta = int(input("Digite a porta: ") or 27017)
    banco = input("Digite o nome do banco: ")
    print(montar_string_conexao(host, porta, banco))


# dunder name
# double underline
if __name__ == '__main__':
    principal()

print(__name__)
