# ESTE ARQUIVO CONTÉM
# !!!SPOILERS!!!

import csv
from datetime import datetime
from os import register_at_fork
from pprint import pprint # pretty print / print bonito

def ler_dados(arquivo: str):
    # [{'Cliente': bla, 'Apolice': bla, 'Validade': bla}]
    resultados = []
    hoje = datetime.now()

    with open(arquivo) as a:
        reader = csv.reader(a)

        headers = next(reader)

        for line in reader:
            registro = dict(zip(headers, line))
            registro['Validade'] = datetime.strptime(registro['Validade'], "%Y-%m")

            if registro['Validade'].month < hoje.month:
                registro['Válido'] = 'EXPIRADO'

            elif registro['Validade'].month > hoje.month:
                registro['Válido'] = 'VÁLIDO'

            else:
                registro['Válido'] = 'A VENCER'

            resultados.append(registro)

    return resultados


def enviar_email(msg, /, *, assunto, destinatario):
    print(f"PARA: {destinatario}")
    print(f"{assunto.upper()}\n")
    print(msg)
    print()

def processar_apolices(*apolices):
    for apolice in apolices:
        if apolice['Válido'] == 'EXPIRADO':
            enviar_email(
                "Por favor renove a sua apólice",
                assunto=f"Apólice vencida: {apolice['Apolice']}",
                destinatario=apolice['Cliente'])

        elif apolice['Válido'] == 'A VENCER':
            enviar_email(
                "Atenção à data de vencimento de sua apólice!",
                assunto=f"Apólice a vencer: {apolice['Apolice']}",
                destinatario=apolice['Cliente'])

apolices = ler_dados('apolices.csv')
processar_apolices(*apolices)
