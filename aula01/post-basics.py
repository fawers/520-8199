def incrementar(n: int) -> int:
    return n+1

def deve_se_alistar(idade: int, genero: str) -> bool:
    idade_alistamento = idade in [17, 18]
    eh_homem = genero == 'm'
    deve_alistar = idade_alistamento and eh_homem
    return deve_alistar

def msg_alistamento(idade: int, genero: str) -> str:
    if genero != 'm':
        return "Você não precisa se alistar."

    if idade < 16:
        m = "Você ainda é novo para se alistar."

    # else if
    elif idade == 16:
        m = 'Você tem um ano antes de se alistar.'

    elif idade > 18:
        m = "Já passou da época! Você se alistou?"

    else:
        m = "Tá na hora de se alistar!"

    return m

# numero = int(input("Digite um número: "))
# print(incrementar(incrementar(numero)))

idade = int(input("Qual é a sua idade? "))
genero = input("Qual é o seu gênero? [m/f/o] ")

# if deve_se_alistar(idade, genero):
#     print("TRUE", "Chegou o ano de se alistar no exército!")

# else:
#     print("FALSE", "Não é ano de se alistar")

print(msg_alistamento(idade, genero))
