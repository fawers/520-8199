class PlacaVeicular:
    COR = 'padrao'
    RESET = '\x1b[0m'
    CORES = {
        'padrao': '\x1b[1;47;30m',
        'taxi': '\x1b[1;41;37m',
        'embaixada': '\x1b[1;44;37m',
        'escola': '\x1b[1;47;31m',
        'colecionador': '\x1b[1;40;37m',
    }

    def __init__(self, placa: str, cor: str = ''):
        self.placa = placa
        self.cor = cor or self.COR

    def mostrar(self):
        placa = self.placa
        print(f"{self.CORES.get(self.cor, '')}{placa}{PlacaVeicular.RESET}")


class PlacaEmbaixada(PlacaVeicular):
    COR = 'embaixada'


class PlacaTaxi(PlacaVeicular):
    COR = 'taxi'


class PlacaAutoEscola(PlacaVeicular):
    COR = 'escola'


class PlacaColecionador(PlacaVeicular):
    COR = 'colecionador'


class PlacaMoto(PlacaVeicular):
    def mostrar(self):
        cima = ' '.join(self.placa[:3])
        baixo = ' '.join(self.placa[3:])
        cor = self.CORES.get(self.cor, '')
        print(f"{cor}{cima:^7}{self.RESET}\n{cor}{baixo}{self.RESET}")


class PlacaMotoEscola(PlacaMoto, PlacaAutoEscola):
    pass


carro_comum = PlacaVeicular('XXX0000')
taxi = PlacaTaxi('TAX1000')
carro_embaixada = PlacaEmbaixada('EMB1337')
moto_escola = PlacaMotoEscola('LEG0420')
colecionador = PlacaColecionador('COL1950')

print(f"{carro_comum.placa=}, {carro_comum.cor=}")
print(taxi.placa)
print(taxi.cor)

carro_comum.mostrar()
taxi.mostrar()
carro_embaixada.mostrar()
moto_escola.mostrar()
colecionador.mostrar()

# method resolution order
# ordem que o python usa para procurar atributos e métodos
print(PlacaMotoEscola.mro())
