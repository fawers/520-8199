def printf_style():
    print("Eu me chamo %s e tenho %s anos de idade." % ('Fabricio', 30))
    print("Dou aula de %(curso)s na %(escola)s há %(tempo)d meses."
          % {'tempo': 14, 'curso': 'Python', 'escola': '4Linux'})

    fim = 100
    for num in range(0, fim+1, 10):
        print("%05d - alinhadaço :D" % (num,))

    pi = 3.14
    print("%(meu_numero_quebrado).100f" % {'meu_numero_quebrado':pi})
    meu_modelo = "%(nome)s %(sobrenome)s"
    meus_dados = {'nome': 'Fabricio', 'sobrenome': 'Werneck'}
    minha_string = meu_modelo % meus_dados
    print(minha_string)


def format_style():
    meu_modelo_posicional = "{1}, {0}"
    meu_modelo_chaveado = "{sobrenome}, {nome}"

    print(meu_modelo_posicional.format("Aline", "Hadad"))
    print(meu_modelo_chaveado.format(sobrenome='Campos', nome="Jessica"))

    v = 2
    while v < 1_000_000:
        print("número: {python_eh_cool:5d}".format(python_eh_cool=v))
        v **= 2  # v = v ** 2


def fstring_style():
    # v = 2
    # while v < 1_000_000_000:
    #     print(f"número: {v:5,d}".replace(',', '.'))
    #     v **= 3  # v = v ** 3

    print(f"| {' ID ':=^8s} | {' Curso ':=^30s} |")

    cursos = [
        (520, 'Python Fundamentals'),
        (202, 'Go Programming'),
    ]

    # [1  , 2 , 3]
    # ['a','b','c']
    # [(1, 'a'), (2, 'b'), (3, 'c')]

    # for curso in cursos:
    for (id, nome) in cursos:
        # id = curso[0]
        # nome = curso[1]
        print(f"| {id:^8d} | {nome:^30s} |")


# printf_style()
# format_style()
fstring_style()
