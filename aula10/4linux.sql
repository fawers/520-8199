-- Structured Query Language

CREATE TABLE IF NOT EXISTS cursos (
    id      INTEGER,
    nome    TEXT
);

CREATE TABLE IF NOT EXISTS turmas (
    id          INTEGER,
    curso_id    INTEGER,
    professor   TEXT
);

CREATE TABLE IF NOT EXISTS membros (
    turma_id    INTEGER,
    nome        TEXT
);
