# Baby Boomer   Até 1964
# Geração X     1965 - 1979
# Geração Y     1980 - 1994
# Geração Z     1995 - Atual

from typing import Callable, List, Tuple
from datetime import date

def metodoI(ano: int) -> str:
    if ano <= 1964:
        return('Baby Boomer')

    elif ano <= 1979:
        return('X')

    elif ano <= 1994:
        return('Y')

    else:
        return('Z')

def metodoII(ano: int) -> str:
    geracoes = {
        'Baby Boomer': 1964,
        'X': 1979,
        'Y': 1994
    }

    for (g, a) in geracoes.items():
        if ano <= a:
            return g
    return 'Z'

def metodoIII(ano: int) -> str:
    geracoes: List[Tuple[Callable[[int], bool], str]] = [
        (lambda a: a <= 1964, 'Baby Boomer'),
        (lambda a: a <= 1979, 'X'),
        (lambda a: a <= 1994, 'Y'),
        (lambda _: True, 'Z'),
    ]

    for (fn, g) in geracoes:
        if fn(ano):
            return g


def main():
    ano = None
    while ano is None:
        try:
            ano = int(input("Digite o seu ano de nascimento: "))
            if ano > date.today().year:
                raise ValueError()
        except ValueError:
            ano = None

    print(f"Você pertence à geração {metodoIII(ano)}.")


if __name__ == '__main__':
    main()
