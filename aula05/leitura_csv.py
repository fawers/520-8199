import io
import csv
import json
from typing import Dict, List, Union

Object = Union[str, int]
def de_csv_para_json(regs: List[Dict[str, str]]) -> List[Dict[str, Object]]:
    conversores = {
        'Ano': lambda text: int(text),
        'Semestre': lambda text: int(text[:-1]),
        'Quantidade de reclamações reguladas procedentes': int,
        'Quantidade de reclamações reguladas - outras': int,
        'Quantidade de reclamações não reguladas': int,
        'Quantidade total de reclamações': int,
        'Quantidade de clientes – Consorciados': int
    }

    j = []
    for reg in regs:
        reg = reg.copy()

        for (coluna, conversor) in conversores.items():
            reg[coluna] = conversor(reg[coluna])

        j.append(reg)

    return j

def processar(arq: io.TextIOWrapper):
    reader = csv.reader(arq, delimiter=';')
    cabecalhos = next(reader)
    registros: List[Dict[str, str]] = []

    for valores in reader:
        reg = dict(zip(cabecalhos, valores))
        reg.pop('', None)
        registros.append(reg)

    return registros


def main():
    # f = open("dataset.csv")
    #                         read
    with open("dataset.csv", 'r', encoding="1252") as f:
        regs = de_csv_para_json(processar(f))
    # f.close()

    regs_veiculos = filter(
        lambda r: any(nome in r['Administradora de consórcio']
                      for nome in ('YAMAHA', 'SUZUKI', 'SCANIA')),
        regs)

    # print(next(regs_porto))
    for reg in regs_veiculos:
        print(reg)

    #                          write
    with open("dataset.json", 'w') as f:
        json.dump(regs, f, indent=4)


if __name__ == '__main__':
    main()
