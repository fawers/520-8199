from typing import Optional

import biblioteca
import excecoes
from menu import Menu


def menu_principal():
    biblio = biblioteca.Biblioteca(
        biblioteca.Livro("Python Fundamentals", 220, ['programação', 'iniciantes']),
        biblioteca.Livro("Programação com Scala", 450, ['funcional', 'programação']))

    menu = Menu(
        "==== Biblioteca ====",
        'listar', 'pesquisar', 'pesquisar por categoria', 'adicionar', 'remover', 'sair')

    while (entrada := menu()) != 'sair':
        if entrada == 'listar':
            listar(biblio)

        elif entrada == 'pesquisar':
            nome = input("Digite o nome do livro para pesquisar: ")
            livro = pesquisar(biblio, nome)
            if livro is not None:
                print(f"Encontrei: {livro}")

            else:
                print(f"Não encontrei o livro {nome}")

        elif entrada == 'pesquisar por categoria':
            categoria = input("Digite a categoria: ")
            livros = biblio.livros_por_categoria(categoria)

            if livros:
                print("Encontrei:", *livros, sep='\n')

            else:
                print(f"Não encontrei nenhum livro com {categoria=}")

        elif entrada == 'adicionar':
            try:
                adicionar(biblio)

            except excecoes.LivroRuimError as lre:
                print(f"Não consegui adicionar o livro {lre.args[0]}:")
                print(f"{lre.args[1]!r} não é um número de páginas válido.")

        elif entrada == 'remover':
            try:
                remover(
                    biblio,
                    input("Digite o nome do livro para remover: "))

            except excecoes.LivroAusenteError as lae:
                print(f"Não existe um livro chamado {lae.args[0]} na biblioteca.")

        print()

    print("\nVolte sempre! Mas em silêncio.")


def listar(b: biblioteca.Biblioteca):
    print("Livros presentes na biblioteca:")

    for livro in b:
        print(livro)


def pesquisar(b: biblioteca.Biblioteca, nome: str) -> Optional[biblioteca.Livro]:
    try:
        return b[nome]

    except KeyError:
        return None


def adicionar(b: biblioteca.Biblioteca):
    nome = input("Digite o nome do novo livro: ")
    pags = input("Digite o número de páginas: ")
    cats = input("Digite as categorias separadas por vírgula: ").split(',')

    try:
        num_pags = int(pags)

    except (ValueError, AssertionError):
        raise excecoes.LivroRuimError(nome, pags)

    else:
        b += biblioteca.Livro(nome, num_pags, cats)


def remover(b: biblioteca.Biblioteca, nome: str):
    try:
        del b[nome]

    except KeyError:
        raise excecoes.LivroAusenteError(nome)


def main():
    menu_principal()


if __name__ == '__main__':
    main()
