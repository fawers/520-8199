def saudar(quem, /, *, onde):
    return f"Olá, {quem} de {onde}!"

print(saudar("Evellyn com 2 Ls", onde="OZ"))

def somatorio(*numeros: int) -> int:
    soma = 0

    for num in numeros:
        soma += num

    return soma

print(somatorio())
print(somatorio(5))
print(somatorio(1,2,3,4,5))

mil_numeros = [1] * 1000
print(somatorio(*mil_numeros))
# print(*([1] * 1000))

def funcao_que_aceita_opcoes(**opcoes):
    print(opcoes)

funcao_que_aceita_opcoes()
funcao_que_aceita_opcoes(chave='valor')
funcao_que_aceita_opcoes(a=1, b=2, c=3)

pessoinha = {'nome': 'Andre', 'sobrenome': 'Firmo'}
funcao_que_aceita_opcoes(**pessoinha)
