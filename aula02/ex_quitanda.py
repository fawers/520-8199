def mostrar_menu(titulo: str, opcoes: list[str]):
    print(titulo)

    for op in opcoes:
        print(f"- {op}")

    entrada = ''

    while entrada not in opcoes:
        entrada = input("Digite a opção desejada: ")

    return entrada

def ver_cesta(cesta: dict[str, int]):
    for fruta in cesta:
        quantidade = cesta[fruta]
        print(f"{quantidade} {fruta}(s)")

    print(f"\ntotal: {len(cesta)} itens")

def adicionar_frutas(quitanda: dict[str, float]) -> str:
    return mostrar_menu("\nFrutas:", list(quitanda))

def checkout(quitanda: dict[str, float], cesta: dict[str, int]) -> float:
    total = 0.0

    for fruta in cesta:
        qtd = cesta[fruta]
        preco = quitanda[fruta]
        total += preco * qtd

    return total

def menu_principal(quitanda: dict[str, float], cesta: dict[str, int]):
    entrada = ''

    while entrada != 'sair':
        entrada = mostrar_menu("Quitanda:\n",
                               ['ver cesta', 'adicionar frutas',
                                'checkout', 'sair'])

        if entrada == 'ver cesta':
            ver_cesta(cesta)

        elif entrada == 'adicionar frutas':
            fruta = adicionar_frutas(quitanda)

            if fruta in cesta:
                cesta[fruta] += 1

            else:
                cesta[fruta] = 1

        elif entrada == 'checkout':
            total = checkout(quitanda, cesta)
            print(f"Total: Py$ {total:.2f}")

    print("Volte sempre!")

def main():
    quitanda: dict[str, float] = {
        'banana': 3.50,
        'melancia': 7.50,
        'morango': 5.00
    }
    cesta: dict[str, int] = {}
    menu_principal(quitanda, cesta)

main()
