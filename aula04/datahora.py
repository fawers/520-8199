from datetime import datetime, timedelta, timezone


def main():
    # australia = timezone(datetime.timedelta(hours=9))
    # california = timezone(datetime.timedelta(hours=-8))
    # agora = datetime.now().astimezone(california)
    # print(agora)

    # delta24h = timedelta(hours=24)
    # agora = datetime.now().astimezone()
    # amanha = agora + delta24h

    agora = datetime.now()
    nascimento = datetime(1992, 4, 4)
    tempo_de_vida = agora - nascimento
    print(tempo_de_vida)

    print(agora.replace(hour=16))
    print(agora.isoformat())


if __name__ == '__main__':
    main()
