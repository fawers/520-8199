def decrementar(n: int) -> int:
    return n - 1

def countdown(n: int):
    while n > 0:
        print(n)
        n = decrementar(n)

def somatorio(n: int) -> int:
    # n = 5
    # S(5) = 1 + 2 + 3 + 4 + 5
    # S(5) = 15
    soma = 0

    while n > 0:
        soma += n
        n = decrementar(n)

    return soma

def produtorio(n: int) -> int:
    # n = 5
    # P(5) = 5 * 4 * 3 * 2 * 1
    # P(5) = 5! = 120
    prod = 1

    while n > 0:
        prod *= n
        n = decrementar(n)

    return prod


def dobrar(nums: list[int]) -> list[int]:
    # PARA CADA num PRESENTE NA NOSSA SEQUÊNCIA / LISTA nums, FAÇA
    dobrados = []

    # foreach
    for num in nums:
        dobrados.append(num * 2)

    return dobrados

def test_range():
    # for (int i = 0; i < 10; i += 1)
    # for i in range(0, 10, 1):
    for i in range(10):
        print(i)

test_range()
