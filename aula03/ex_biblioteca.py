from typing import Dict, List, Union, Optional

# Union: um dos tipos especificados
Livro = Dict[str, Union[str, int, List[str]]]
Biblioteca = List[Livro]

def mostrar_menu(titulo: str, opcoes: List[str]):
    print(titulo)

    for op in opcoes:
        print(f"- {op}")

    entrada = ''

    while entrada not in opcoes:
        entrada = input("Digite a opção desejada: ")

    return entrada

def novo_livro(nome: str, num_pags: int, categorias: List[str]) -> Livro:
    livro: Livro = {'nome': nome}
    livro.update(num_pags=num_pags, categorias=categorias)
    return livro

def criar_biblioteca(*livros: Livro) -> Biblioteca:
    return list(livros)

def listar(biblioteca: Biblioteca):
    livro_str = '{} ({}p)\n{}\n'

    for livro in biblioteca:
        print(livro_str.format(livro['nome'], livro['num_pags'],
                               ', '.join(livro['categorias'])
                              )
             )

# Optional: o tipo especificado ou None
def pesquisar(biblioteca: Biblioteca, nome: str) -> Optional[Livro]:
    nome = nome.lower()

    for livro in biblioteca:
        if livro['nome'].lower() == nome:
            return livro

    return None

def adicionar(biblioteca: Biblioteca, livro: Livro):
    biblioteca.append(livro)
    # biblioteca.extend([livro])


def remover(biblioteca: Biblioteca, nome_livro: str) -> bool:
    livro = pesquisar(biblioteca, nome_livro)

    if livro is not None:
        biblioteca.remove(livro)
        return True

    return False

def menu_principal():
    biblioteca = criar_biblioteca(
        novo_livro("Black Hat Python", 360, ['programação', 'hacking']),
        novo_livro("Scala For The Impatient", 400, ['iniciantes', 'programação']))

    entrada = ''
    while entrada != "sair":
        # listem, adicionem, removam, pesquisem livros
        entrada = mostrar_menu(
            "===== Biblioteca =====",
            ['listar', 'pesquisar', 'adicionar', 'remover', 'sair'])

        if entrada == 'listar':
            listar(biblioteca)

        elif entrada == 'pesquisar':
            nome_livro = input("Digite o nome do livro desejado: ").strip()
            livro = pesquisar(biblioteca, nome_livro)

            if livro is not None:
                print(f"Encontrei: {livro['nome']}, {livro['num_pags']} páginas.")

            else:
                print(f"Não encontrei nenhum livro que se chama {nome_livro}. :(")

        elif entrada == 'adicionar':
            livro = novo_livro(
                input("Digite o nome do livro: "),
                int(input("Digite o número de páginas: ")),
                input("Digite as categorias separadas por vírgula: ").split(','))
            adicionar(biblioteca, livro)

        elif entrada == 'remover':
            nome_livro = input("Digite o nome do livro desejado: ").strip()

            if remover(biblioteca, nome_livro):
                print(f"{nome_livro} removido da biblioteca.")

            else:
                print(f"Não foi possível remover {nome_livro} da biblioteca. :(")

        print()


menu_principal()
