def add(x, y):
    return x + y

def sub(x, y):
    return x - y

def mul(x, y):
    return x * y

def main():
    print("Bem-vindo à calculadora do Helder!")
    print("Que operação vc quer fazer hoje?")

# standalone
if __name__ == '__main__':
    main()
