def ex01():
    cabecalho = '-----------------------------'
    rodape = cabecalho

    nome = input("Nome:  ")
    cpf = input("CPF:   ")
    idade = input("Idade: ")
    uf = input("UF:    ")

    print(cabecalho)
    print("Confirmação de cadastro:")
    #     ↓ f de formatado
    print(f"Nome:  {nome}")
    print(f"CPF:   {cpf}")
    print(f"Idade: {idade}")
    print(rodape)


def ex02():
    def substituir(frase: str, a: str, b: str) -> str:
        return frase.replace(a, b)

    print(substituir('Um|prato|de|trigo|para|três|tigres|tristes',
                     '|', ' '))


def ex03():
    cabecalho = '------------------------------'
    a = int(input('a = '))
    b = int(input('b = '))

    print(cabecalho)
    print("Soma     : %(a)d + %(b)d = %(a + b)d" % {'a': a, 'b': b, 'a + b': a + b})
    # print("Diferença: {a} - {b} = {a - b}".format(a=a, b=b, **{'a - b': a-b}))
    # print("Diferença: {a} - {b} = {a - b}".format(**{'a':a, 'b':b,'a - b': a-b}))
    print("Diferença: {a} - {b} = {a - b}".format_map({'a':a, 'b':b,'a - b': a-b}))


if __name__ == '__main__':
    ex01()
    ex02()
    ex03()
