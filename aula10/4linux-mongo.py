# https://www.mongodb.com/docs/manual/reference/operator/query/

# pip install pymongo
# apostila:
# pip install montydb
import json

from pymongo import MongoClient
# from montydb import MontyClient as MongoClient

def primeira_execucao():
    with open('4linux-mongo.json') as json_file:
        dados = json.load(json_file)

    client = MongoClient()
    db = client['4linux']

    for collection_name in ('cursos', 'turmas', 'membros'):
        col = db[collection_name]

        if col.count_documents({}) > 0:
            client.close()
            return False

        col.insert_many(dados[collection_name])

    client.close()
    return True

# 8199 8764
def get_turmas():
    client = MongoClient()

    db = client['4linux']

    turma = db['turmas'].find_one({
        'professor': 'Fabricio Werneck'
    })

    membros = db['membros'].find({
        'turma': {'$in': [
            turma['id'],
            8764
        ]}
    })

    for membro in membros:
        print(membro)

    client.close()

def main():
    print(f"{primeira_execucao()=}")
    get_turmas()

if __name__ == '__main__':
    main()
