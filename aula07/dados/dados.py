import random

import excecoes


# pense dado do banco imobiliário (jogo), não de registros
class Dado:
    def __init__(self, faces: int = 6):
        if faces == 0:
            raise excecoes.FaceZeroError()

        elif faces < 0:
            raise excecoes.FaceNegativaError(faces)

        self.faces = faces
        self.r = random.Random()

    def rolar(self) -> int:
        return self.r.randint(1, self.faces)

    # equivalente à função novo_dado_com_semente abaixo
    # alternativa ao __init__
    # @classmethod
    # def com_semente(cls, semente: int, faces: int = 6):
    #     dado = cls(faces)
    #     # mesma coisa que
    #     # dado = Dado(faces)

    #     dado.r.seed(semente)
    #     return dado

def novo_dado_com_semente(semente: int, faces: int = 6):
    dado = Dado(faces)
    dado.r.seed(semente)
    return dado


class DadoViciado(Dado):
    def rolar(self):
        return self.faces


def main():
    d = novo_dado_com_semente(5)
    for _ in range(6):
        print(d.rolar())


def main2():
    d = DadoViciado(21)
    for _ in range(7):
        print(d.rolar())


def main3():
    try:
        Dado(-1)

    # except excecoes.FaceNegativaError as fne:
    #     print(f"pq vc quer criar um dado com {fne} faces?")

    # except excecoes.FaceZeroError:
    #     print("dados de 0 faces não existem nesse plano")

    except excecoes.DadoError as de:
        print(f"{de=}")


def main4():
    def rolamentos(d: Dado):
        rolls = [True] + [False] * (d.faces)
        count = 0

        while not all(rolls):
            roll = d.rolar()
            count += 1
            rolls[roll] = True

        return count

    for seed in range(100):
        d21 = novo_dado_com_semente(faces=21, semente=seed)
        print(f"{seed=} {rolamentos(d21)=}")


if __name__ == '__main__':
    main4()
