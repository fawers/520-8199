class Menu:
    def __init__(self, titulo: str, *opcoes: str):
        self.titulo = titulo
        self.opcoes = opcoes

    def mostrar(self):
        print(self.titulo)
        print()

        for (i, opcao) in enumerate(self.opcoes, start=1):
            print(f"{i}. {opcao}")

    def receber_entrada(self) -> str:
        entrada = -1
        intervalo = range(1, len(self.opcoes)+1)

        while entrada not in intervalo:
            try:
                entrada = int(input("Digite a opção desejada: "))
            except:
                pass

        return self.opcoes[entrada-1]

    def __call__(self) -> str:
        self.mostrar()
        return self.receber_entrada()
