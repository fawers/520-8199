import json
from pprint import pprint

def ler_json(nome_arq: str):
    with open(nome_arq) as f:
        registros = json.load(f)

    return registros

def main():
    regs = ler_json("dataset.json")
    pprint(regs[0])

if __name__ == '__main__':
    main()
